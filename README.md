# Example about using ELK and FAST API with the Space resource

## Author
- [José Danilo Sánchez Torres]

## Prerrequisites
For the installation of this solution the following applications are required to be installed on a WINDOWS or Linux operating system:

- [Python](https://www.python.org/ftp/python/3.10.7/python-3.10.7-amd64.exe) - PYTHON v.3.10 (including PIP and adding to PATH)
- [Pycharm](https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=windows&code=PCC) - Pycharm 2022.2.1 (Community Edition)
- [Kibana](http://localhost:5601/) - Kibana 8.5.0 (Community Edition)

## Installation of solution dependencies

This application is based on the use of some libraries (modules) in order to carry out the execution of the graphical interface component related to the initiative.

| Dependencies | Purpose |
| ------ | ------ |
| FastAPI| Microframework used for the management of APIs related to ELK components |
| Uvicorn | Library used to deploy the APIs related to the example from FAST API Framework |
| Requests | Library for handling HTTP request types in the software component |


The following instructions should then be executed via command line

Installing FAST API

```sh
pip install fastapi
```

Installing requests

```sh
 pip install requests
```

Installing Uvicorn

```sh
  pip install uvicorn
```


## Deploying solution

Once the source code has been downloaded, the following instruction must be executed, either from the integrated development environment called Pycharm or directly from the root folder of the project, using the following command:

```sh
 uvicorn main:jdst_globant_tech_talk --reload --port 9092
```
By default, the system will use the URL localhost(127.0.0.1) and port 8000, however, for this occasion port 9092 will be used.

```sh
127.0.0.1:9092
```

## Execution and example

For executing the solution, you must use the URL http://localhost:9092/docs 

![FAST API SPACE RESOURCE](docs/docspage.png "FAST API SPACE")

Then, inside the POST service, you could add the next body structure similar to the following example

```json
{
  "id": "techtalk2022jdst",
  "name": "techtalk2022jdst",
  "description": "This is a test about techtalk2022jdst",
  "color": "#bfd732",
  "initials": "J"
}
```

Finally, you can execute a request as the following image:

![EXECUTE POST SPACE SERVICE](docs/executePostSpaceService.png "EXECUTE POST SPACE SERVICE")

As a result, you will be able to visualize the creation of a space from an existing Kibana.

![EXECUTE POST SPACE SERVICE](docs/selectMySpace.png "EXECUTE POST SPACE SERVICE")

## License
MIT

