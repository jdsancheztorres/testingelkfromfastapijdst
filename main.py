"""
    Example about using ELK and FAST API with the Space resource
    Tech Talk - 17112022
    Glober: José Danilo Sánchez Torres.
"""
from fastapi import FastAPI
from api.api import router as api_router

jdst_globant_tech_talk = FastAPI();
jdst_globant_tech_talk.include_router(api_router)