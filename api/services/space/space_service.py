from api.services.space.space_service_wrapper import add_space_detail;
from api.payloads.request import Request_JDST;

def add_space(request: Request_JDST):
    response = None;
    try:
        response = add_space_detail(request);
    except Exception as ex:
        response = "The space with id " + request.id_proveedor + " was not created - KO";

    return response;