from api.payloads.request import Request_JDST, RestAPIRequest;
from api.payloads.response import response_JDST
from infra.common.config import properties;
from base64 import b64encode
from infra.client.restclient import invoke_web_service;
'''
{
  "id": "techtalk2022jdst",
  "name": "techtalk2022jdst",
  "description": "This is a test about techtalk2022jdst",
  "color": "#bfd732",
  "initials": "J"
}
'''
def add_space_detail(request: Request_JDST):

    url = properties['kibanaAPIUrl'] + "/api/spaces/space";
    username = properties['userElastic'];
    password = properties['passwordElastic'];
    my_bytes = b'';
    data = username + ":" + password;
    result = my_bytes + data.encode('utf-8');
    credentials = b64encode(result);
    payload = {
        "id" : request.id,
        "name": request.name,
        "description" : request.description,
        "color" : request.color
    };
    headers = {'kbn-xsrf': properties['kbn-xsrf-kibana'],
               "Authorization": "Basic " + credentials.decode("utf-8")};
    restAPIRequest = RestAPIRequest();
    restAPIRequest.url = url;
    restAPIRequest.payload = payload;
    restAPIRequest.username = username;
    restAPIRequest.password = password;
    restAPIRequest.headers = headers;
    answer = invoke_web_service(restAPIRequest);

    return response_JDST(answer, 200, "Space added successfully.", False)