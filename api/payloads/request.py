from pydantic import BaseModel,Field

class Request_JDST(BaseModel):
    id: str = Field(None, title="ID", description="Space ID")
    name: str = Field(None, title="Name of Space", max_length=100)
    description: str = Field(None, title="Description related to the space", max_length=100)
    color: str = Field(None, title="Color", max_length=100)
    initials: str = Field(None, title="Initials", max_length=100)

class RestAPIRequest(BaseModel):
    url: str = Field(None, title="ID", description="URL")
    username: str = Field(None, title="Usename", max_length=100)
    password: str = Field(None, title="Password", max_length=100)
    payload: str = Field(None, title="Payload", max_length=100)
    headers: str = Field(None, title="Headers", max_length=100)