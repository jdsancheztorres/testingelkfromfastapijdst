from fastapi import APIRouter
from api.elk import space;

router = APIRouter();
router.include_router(space.router);