from api.services.space.space_service import add_space_detail;
from api.payloads.request import Request_JDST;
from fastapi import APIRouter

router = APIRouter(
    prefix="/spaces",
    tags=["Space"],
    responses={404: {"description": "Not found"}},
)

@router.post("/component", response_description="Add component")
async def add_component(request: Request_JDST):
    response = None;
    try:
        response = add_space_detail(request);
        print("response")
    except Exception as ex:
        response_message = "The space with id " + str(request.id) + " was not created succesfully - KO";
        print(ex);
    return response;

