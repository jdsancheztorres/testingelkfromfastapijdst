import requests,json;
from api.payloads.request import RestAPIRequest;

def invoke_web_service(request: RestAPIRequest):

    answer = requests.post(request.url,
                           auth=(request.username, request.password),
                           data=json.dumps(request.payload), headers=request.headers)
    return answer;